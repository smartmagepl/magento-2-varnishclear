# README #

### SmartMage VarnishClear ###

Moduł do czyszczenia cache Varnish-a z poziomu panelu administracyjnego. W zarządzaniu pamięcią podręczną, w sekcji "Dodatkowe opcje" pojawią się dodatkowe przyciski. 
"Purge Varnish Domain Cache" czyści cały cache Varnish-a, "Purge Varnish Url Cache" czyści tylko wybrany url wpisany w polu tekstowym, np: pub/media/styles.css.

### Instalacja ###

Pliki moduły wgrywamy do katalogu /app/code/Smartmage/VarnishClear

Następnie w konsoli uruchamiamy następujące komendy:

* php bin/magento module:enable Smartmage_VarnishClear
* php bin/magento setup:upgrade
* php bin/magento setup:di:compile
* php bin/magento setup:static-content:deploy pl_PL
* php bin/magento cache:flush

Konfiguracja modułu znajduje się w Sklepy -> Konfiguracja -> Zaawansowane -> System -> Pamięć podręczna stron -> Konfiguracja Varnish.
Wymagane jest podanie kodu "Varnish Authentication Key". Można go pobrać z serwera z pliku /etc/varnish/secret. Na końcu kodu należy umieścić znak końca lini "\n" np: "62e3b2d2-372d-4562-83ce-b3196773362c\n"
