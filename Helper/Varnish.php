<?php

namespace Smartmage\VarnishClear\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\StoreManagerInterface;
use \Magento\Framework\Message\ManagerInterface;

class Varnish extends \Magento\Framework\App\Helper\AbstractHelper
{
	protected $_storeManager;

	protected $_scopeConfig;

	protected $_messageManager;

	protected $fp;

	protected $host;

	protected $port;

	protected $secret;

	protected $ban = 'ban';

	public function __construct(
		StoreManagerInterface $storeManager,
		ScopeConfigInterface $scopeConfig,
		ManagerInterface $messageManager
	){
		$this->_scopeConfig = $scopeConfig;
		$this->_storeManager = $storeManager;
		$this->_messageManager = $messageManager;

		$host = $this->_scopeConfig->getValue(
			'system/full_page_cache/varnish/backend_host',
			\Magento\Store\Model\ScopeInterface::SCOPE_STORE,
			$this->_storeManager->getStore()->getId());

		$varnish_port = $this->_scopeConfig->getValue(
			'system/full_page_cache/varnish/varnish_port',
			\Magento\Store\Model\ScopeInterface::SCOPE_STORE,
			$this->_storeManager->getStore()->getId());

		$secret_key = $this->_scopeConfig->getValue(
			'system/full_page_cache/varnish/secret_key',
			\Magento\Store\Model\ScopeInterface::SCOPE_STORE,
			$this->_storeManager->getStore()->getId());

		$this->host = $host ? $host : '127.0.0.1';
		$this->port = $varnish_port ? $varnish_port : 6082;
		$this->secret = $secret_key ? $secret_key : '';
		$this->secret = str_replace('\n', PHP_EOL, $this->secret);

		if(!$this->secret){
			throw new \Exception('Varnish Authentication Key is Required');
		}

		$this->connect();
	}

	public function connect($timeout = 5){
		$this->fp = fsockopen($this->host, $this->port, $errno, $errstr, $timeout);
		if(!is_resource($this->fp)){
			throw new \Exception(__('Failed to connect to varnishadm on %1:%2; "%3"', $this->host, $this->port, $errstr));
		}
		// set socket options
		stream_set_blocking($this->fp, 1);
		stream_set_timeout($this->fp, $timeout);
		// connecting should give us the varnishadm banner with a 200 code, or 107 for auth challenge
		$banner = $this->read($code);
		if( $code === 107 ){
			if(!$this->secret){
				throw new \Exception(__('Authentication required; Check the Varnish Authentication Key'));
			} try {
				$challenge = substr($banner, 0, 32);
				$response = hash('sha256', sprintf("%s\n%s%s\n", $challenge, $this->secret , $challenge));
				$banner = $this->command('auth '.$response, $code, 200);
			} catch(\Exception $Ex){
				var_dump($Ex->getMessage());
				throw new \Exception(__('Varnish Authentication failed'));
			}
		}
		if($code !== 200){
			throw new \Exception(__('Bad response from varnishadm on %1:%2', $this->host, $this->port));
		}
		return $banner;
	}

	private function write($data){
		$bytes = fputs($this->fp, $data);
		if($bytes !== strlen($data)){
			throw new \Exception(__('Failed to write to varnishadm on %1:%2', $this->host, $this->port));
		}
		return true;
	}

	public function command($cmd, &$code, $ok = 200){
		$cmd and $this->write($cmd);
		$this->write("\n");
		$response = $this->read($code);
		if($code !== $ok){
			$response = implode("\n > ", explode("\n",trim($response)));
			throw new \Exception(__("%1 command responded %2:%3", $cmd, $code, $response), $code);
		}
		return $response;
	}

	private function read(&$code){
		$code = null;
		// get bytes until we have either a response code and message length or an end of file
		// code should be on first line, so we should get it in one chunk
		while(!feof($this->fp)){
			$response = fgets($this->fp, 1024);
			if(!$response){
				$meta = stream_get_meta_data($this->fp);
				if($meta['timed_out']){
					throw new \Exception(__('Timed out reading from socket %1:%2',$this->host,$this->port));
				}
			}
			if(preg_match('/^(\d{3}) (\d+)/', $response, $r)){
				$code = (int)$r[1];
				$len = (int)$r[2];
				break;
			}
		}
		if(is_null($code) ){
			throw new \Exception(__('Failed to get numeric code in response'));
		}
		$response = '';
		while (!feof($this->fp) && strlen($response) < $len){
			$response .= fgets($this->fp, 1024);
		}
		return $response;
	}

	public function close(){
		is_resource($this->fp) and fclose($this->fp);
		$this->fp = null;
	}

	public function quit(){
		try {
			$this->command('quit', $code, 500);
		}
		catch(\Exception $Ex){
			// slient fail - force close of socket
		}
		$this->close();
	}

	public function purgeHost($host){
		try {
			$host = str_replace('https://', '', $host);
			$host = str_replace('http://', '', $host);
			$host = str_replace('www', '', $host);
			$result = $this->command( $this->ban.' req.url ~ .', $code);

			$this->_messageManager->addSuccess(__('Varnish of domain "%1" has purge.', $host));

			return $result;
		} catch(\Exception $e){
			$this->_messageManager->addError($e->getMessage());
		}
	}

	public function purgeUrl($url){
		try {
			$result = $this->command( $this->ban.' req.url ~ '.$url, $code);

			$this->_messageManager->addSuccess(__('URL "%1" has purge.', $url));

			return $result;
		} catch(\Exception $e){
			$this->_messageManager->addError($e->getMessage());
		}
	}

	public function status(){
		try {
			$response = $this->command('status', $code);
			if(!preg_match('/Child in state (\w+)/', $response, $r)){
				return false;
			}
			return $r[1] === 'running';
		} catch(\Exception $Ex){
			return false;
		}
	}

	public function getBaseUrl(){
		$baseUrl = $this->_storeManager->getStore()->getBaseUrl();
		$baseUrl = str_replace('http://', '', $baseUrl);
		$baseUrl = str_replace('htts://', '', $baseUrl);
		$baseUrl = rtrim($baseUrl, '/');

		return $baseUrl;
	}
}