<?php

namespace Smartmage\VarnishClear\Observer\Catalog;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;
use Smartmage\VarnishClear\Helper\Varnish;
use Magento\Catalog\Api\CategoryRepositoryInterface;

class ClearVarnishAfterProductSave implements ObserverInterface
{
	private $varnish;
	protected $categoryRepository;

	public function __construct(
		Varnish $varnish,
		CategoryRepositoryInterface $categoryRepository
	) {
		$this->varnish = $varnish;
		$this->categoryRepository = $categoryRepository;
	}

	public function execute(EventObserver $observer)
	{
		$product = $observer->getEvent()->getProduct();

		$url = '^/$|';
		$url .= $product->getUrlKey().'|';
		$url .= $product->getUrlPath().'|';
		$url .= '^/'.$product->getUrlKey().'|';
		$url .= '^/'.$product->getUrlPath();

		foreach($product->getCategoryCollection() as $cat){
			$category = $this->categoryRepository->get($cat->getId());
			$url .= '|'.$category->getUrlKey().'|^/'.$category->getUrlKey();
			$url .= '|'.$category->getUrlPath().'|^/'.$category->getUrlPath();
		}

		try{
			$this->varnish->purgeUrl($url);
		} catch (\Exception $e){

		}
	}
}
