<?php

namespace Smartmage\VarnishClear\Controller\Adminhtml\Clear;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Smartmage\VarnishClear\Helper\Varnish;

class Url extends \Magento\Backend\App\Action
{
	protected $resultPageFactory;
	protected $varnish;

	public function __construct(
		Context $context,
		PageFactory $resultPageFactory,
		Varnish $varnish
	){
		$this->resultPageFactory = $resultPageFactory;
		$this->varnish = $varnish;
		parent::__construct($context);
	}

	public function execute()
	{
        if (extension_loaded('Zend OPcache'))
        {
            opcache_reset();
        }

		$this->varnish->purgeUrl($this->getRequest()->getParam('varnish_url'));

		$this->_redirect('adminhtml/cache/index');
	}
}