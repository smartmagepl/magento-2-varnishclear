<?php

namespace Smartmage\VarnishClear\Block\Adminhtml;

class VarnishClear extends \Magento\Backend\Block\Template
{

	public function getPurgeHostLink()
	{
		return $this->getUrl('varnishclear/clear/host');
	}

	public function getPurgeUrlLink()
	{
		return $this->getUrl('varnishclear/clear/url');
	}
}
